; Exemple de fonctions r�cursives retournant un r�sultat dans AX:

               ; Fonction Factorielle(x)
               ; Fonction Puissance(a,x)

DISPLAY MACRO Msg  ;; Affiche une chaine de cract�res qui se termine par un $;
                   ;; l'adresse du d�but de la cha�ne se trouve dans DS:DX
       Push Ax
       Push Dx
       Mov Ah,9    ;; Fonction d'affichage n�9 du DOS
       Lea Dx,Msg  ;; DX <-- offset de la cha�ne
       Int 21h     ;; Appel au DOS en appelant l'interruption 21h
       Pop Dx
       Pop Ax
ENDM

LIRE_CAR MACRO c  ;; Lit un caract�re et met son code ascii dans Bl et l'affiche
       Push Ax   ;; Sauve AL sur la pile
       Mov Ah,1  ;; Fonction de lecture d'un caract�re (fonction n�1 du DOS)
       Int 21h   ;; Le DOS renvoie dans AL le code Ascii du caract�re lu
       Mov c,Al  ;; Sauve Al dans n car on veut que cette Macro ne modifie pas AL
       Pop Ax    ;; Restaure AL
ENDM


Esc   EQU 27 ; d�finition de la constante ESCAPE = 27
Enter EQU 13

PILE SEGMENT STACK   ; d�finition du segment de pile
       DW 1024 DUP(0) ; r�serve 1024 mots initialis�s � 0 pour le segment de pile
PILE ENDS            ; fin du segment de pile

DATA SEGMENT
       In_Arg_Fact DB 13,10,'Entrez l''argument de la factorielle: $'
       Fact_Msg  DB 13,10,'Factorielle($'
       Arg_fact  DW  0
       N         DW  0
       Puiss_Msg DB 13,10,'Puissance($'
       Base      DW  0
       Virgule   DB ',$'
       Exposant  DW  0
       ParEgal   DB ') = $'
       In_Base   DB 13,10,'Entrez la base: $'
       In_Exp    DB 13,10,'Entrez l''exposant: $'
       Msg_Quit  DB 13,10,'Pour Quitter, Appuyer sur Esc: $'
       Msg_Error     DB 13,10,'Le nombre entr� est trop grand:$'
DATA ENDS

CODE SEGMENT ; d�finition du segment de code
       ASSUME CS:CODE, DS:DATA

Factorielle Proc Near
  ; Fonction r�cursive calculant la factorielle de n (retourn�e dans AX) selon
  ; l'algorithme suivant:
  ;                  f(x):
  ;                     Si x <= 1 Alors f<-1
  ;                               Sinon f <- f(x-1) * x
  ;                     Fsi;
  ;  Si le r�sultat est trop grand ( >= 2**16) f(x) retourne 0.

  X Equ [Bp+4]

  Push Bp
  Mov  Bp,Sp
  Push Bx

  Cmp Word Ptr X, 1
  JA AppelFact
  Mov Ax,1
  Jmp FinFact
 AppelFact: Mov Bx,X
            Dec Bx
            Push Bx
            Call Factorielle
            Mov Bx,X
            Mul Bx
            Jnc FinFact
            Xor Ax,Ax
 FinFact: Pop Bx
          Pop Bp
          Ret 2
Factorielle Endp

Puissance Proc Near
  ; Fonction r�cursive calculant la puissance de x de a (retourn�e dans AX) selon
  ; l'algorithme suivant:
  ;   p(a,x) { a>0, x>=0 }
  ;     Si x = 0 Alors p<-1
  ;              Sinon Si x est pair Alors P <- P(a,x div 2) * P(a,x div 2)
  ;                                  Sinon P <- a * P(a,x div 2) * P(a,x div 2)
  ;                    Fsi
  ;     Fsi
  ;   Si le r�sultat est trop grand ( >= 2**16) p(a,x) retourne 0.

  x Equ Word Ptr [Bp+4]
  a Equ Word Ptr [Bp+6]

  Push Bp
  Mov  Bp,Sp
  Push Bx

  Cmp x, 0
  JA Calcul
  Mov Ax,1
  Jmp FinPuiss
 Calcul: Mov BX,x
         Shr BX,1 ; BX <- X Div 2
         JnC Pair; teste si pair ou impair
        Impair: Push a
                Push Bx
                Call Puissance
                Mul Ax ; Ax <-Ax*Ax = Puissance(a,Bx) * Puissance(a,Bx)
                Jc TropGd
                Mul a  ; Ax * a
                Jc TropGd
                Jmp FinPuiss
          Pair: Push a
                Push Bx
                Call Puissance
                Mul Ax ; Ax <-Ax*Ax = Puissance(a,Bx) * Puissance(a,Bx)
                JNc FinPuiss
 TropGd  : Xor Ax,Ax
 FinPuiss: Pop Bx
           Pop Bp
           Ret 4
Puissance Endp

Lis_Entier Proc Near
 ; Cette proc�dure (fonction) lit un entier nom sign� � partir du clavier en
 ; ignorant les caract�res erronn�s et retourne la valeur enti�re dans AX.
 ; Si le nombre entr� est trop grand le CF est mis � 1 (sinon � 0).

   ; Initialisation de la boucle
       Mov Cx,10      ; Cx <-- 10 pour la multiplication par 10
       Mov Bh,0
       Mov Ax,0       ; Au d�part on suppose que le nombre (AX) = 0

  BoucleLire: Lire_car Bl
          Cmp Bl, Enter
          Je End_BoucleLire
          Cmp Bl,'0' ; Si on entre autre chose qu'un chiffre, on l'ignore
          JB BoucleLire  ; Si Bl < '0' on resaute � boucle
          Cmp Bl,'9'
          JA BoucleLire ; Si Bl > '9' on resaute � boucle
          Mul Cx     ; DX:AX <-- 10*AX
          JC End_BoucleLire ; Si CF=1, c'est que le nombre est trop grand
          Sub Bl,'0' ; Bl <--Bl - '0'  pour obtenir le nombre associ� au chiffre Bl
          Add Ax,Bx  ; AX <-- Ax+Bl (BX = BL car on a initialis� BH � z�ro)
          JNC BoucleLire
  End_BoucleLire: Ret
Lis_Entier Endp

Ecris_Entier Proc Near
 ; Cette proc�dure affiche sous forme d�cimale l'entier non sign� Entier pass�
 ; comme param�tre (� la position courante du curseur).

   Entier Equ Word Ptr [BP+4]

;  Conversion du r�sultat Entier sous forme de chiffres d�cimaux
;  en vue de l'affichage.
        Push Bp
        Mov Bp,Sp
        Mov Ax,Entier
        Mov Bx,10
        Mov Cx,0   ; Cx = compteur de chiffres associ�s au nombre

  Bcl1: Xor Dx,Dx
        Inc Cx
        Div Bx     ; Dx = (Dx:Ax) mod 10  et Ax = (Dx:Ax) div 10
        Add Dl,'0' ; convertit le nombre (0 � 9) en un chiffre ('0' � '9')
        Push Dx    ; Sauve temporairement le chiffre sur la pile
        Cmp Al,0
       Jne Bcl1; Si le quotient entier est <>0 on resaute � Boucle1

  Bcl2: Pop Dx          ; D�pile les chiffres empil�s dans Boucle2
        Mov Ah,2                ; Dl = code ascii du chiffre � afficher
        Int 21h         ; Affiche le chiffre contenu dans Dl (fonction n�2 du
                           ; DOS appel�e par l'interruption 21h)
       Loop Bcl2    ; Cx <--Cx - 1. Si Cx <> 0 au resaute � Boucle2
        Pop Bp
        Ret 2
Ecris_Entier Endp

; D�but du programme (Begin = point d'entr�e).
Begin: Mov Ax,DATA ; Fait pointer DS vers le d�but du segment Data ce qui permet
       Mov Ds,Ax   ; d'adresser les objets d�finis dans ce segment.

  MainBoucle:

     ; Saisie, calcul et affichage de Puissance(base,exposant)
           Display in_Base
           Call Lis_entier
           Jnc l1
           Jmp Error
        l1:Mov Base,Ax     ; Saisie de la Base

           Display in_Exp
           Call Lis_entier
           Jnc l2
           Jmp Error
        l2:Mov Exposant,Ax ; Saisie de l'exposant

           Push Base
           Push Exposant
           Call Puissance  ; Calcul de Puissance(Base,exposant) dans Ax
           Mov N,Ax

           Display Puiss_Msg
           Push Base
           Call Ecris_Entier
           Display Virgule
           Push Exposant
           Call Ecris_entier
           Display ParEgal
           Push N
           Call Ecris_Entier ; Affichage du r�sultat

     ; Calcul de la factorielle
           Display In_Arg_Fact
           Call Lis_Entier
           Jc Error
           Mov Arg_Fact,Ax   ; Saisie de Arg_Fact

           Push Ax
           Call Factorielle  ; Calcul de Factorielle(Arg_Fact) dans Ax
           Mov N,Ax

           Display Fact_Msg
           Push Arg_Fact
           Call Ecris_Entier
           Display ParEgal

           Push N
           Call Ecris_Entier ; Affichage du r�sultat
           Jmp Fin
  Error:  Display Msg_Error
  Fin:    Display Msg_Quit
          Lire_Car bl
          Cmp Bl,Esc
          Je Fini
          Jmp MainBoucle
      fini:
          Mov Ah,4Ch  ; Fonction n�4Ch du DOS qui permet de retourner au
                      ; programme appelant (habituellement le DOS)
          Int 21h     ; Appel au Dos

CODE ENDS             ; Fin du segment CODE
       END Begin      ; Fin de la source, Begin d�signant � l'assembleur le
                      ; point d'entr�e dans le programme.