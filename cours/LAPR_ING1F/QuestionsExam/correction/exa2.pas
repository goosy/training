program exa2 ;
uses SysUtils ;

type  	Liste = ^Cellule ;
	Cellule = record
	  clef : String ;
	  num  : Cardinal ;
	  svt  : Liste
	end{Cellule} ;
	ListeTriee = Liste ;

procedure delFirst(var L : Liste) ;
// L <> nil
var p : Liste ;
begin
  p := L ; L := L^.svt ; dispose(p)
end ;

{
procedure pack(var L : ListeTriee) ;
var p,c : Liste ;
begin
  p := L ;
  while p <> nil do
  begin
    c := p^.svt ;
    while (c <> nil) and (c^.clef = p^.clef) do
    begin
      inc(p^.num,c^.num) ;
      delFirst(p^.svt) ;
      c := p^.svt
    end ;
    p := p^.svt
  end
end ;
}

procedure pack(var L : ListeTriee) ;
begin
  if (L <> nil) and (L^.svt <> nil) then
    if L^.clef = L^.svt^.clef then
      begin
        inc(L^.num,L^.svt^.num) ;
        delFirst(L^.svt) ;
        pack(L)
      end
    else pack(L^.svt)
end ;


//-----------------------------------------------
procedure init(out L : Liste) ;
begin
  L := nil
end ;

procedure add(c : String; n : Cardinal ; var L : Liste) ;
var nv : Liste ;
begin
  new(nv) ; nv^.clef := c ; nv^.num := n ; nv^.svt := L ; L := nv
end ;

procedure print(L : Liste) ;
begin
  while L <> nil do
  begin
    write('['+L^.clef+' '+intToStr(L^.num)+']') ;
    L := L^.svt
  end ;
  writeLn
end ;
//-----------------------------------------------

var T : Liste ;
begin
  init(T) ;
  add('F',2,T) ;add('F',6,T) ;
  add('C',7,T) ;
  add('A',3,T) ;add('A',2,T) ;add('A',1,T) ;
  
  print(T) ;
  
  pack(T) ;
  print(T) ;
  
end.
