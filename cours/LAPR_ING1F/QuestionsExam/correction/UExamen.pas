unit UExamen ;

interface
type
      Element = Integer ;      
      PointeurCellule = ^Cellule ;
      Cellule =  record
                   info : Element ;
                   svt  : PointeurCellule
                 end ;
      Liste = PointeurCellule ;

   procedure create(out s : Liste) ; 
   procedure insert(p : PointeurCellule ; var s : Liste) ;
   function delete(var L : Liste) : Liste ;  
   function isEmpty(s : liste) : Boolean ;
   procedure afficher(s : Liste) ;
   procedure addFirst(x : Element ; var L : Liste) ;

   
implementation

procedure create(out s : Liste) ;
begin
  s := nil 
end ;

procedure addFirst(x : Element ; var L : Liste) ; 
var nv : Liste ;
begin
  new(nv) ; nv^.info := x ; nv^.svt := L ; L := nv
end ;

procedure delFirst(var L : Liste) ;
var poubelle : PointeurCellule ;
begin
  poubelle := L ; L := L^.svt ; dispose(poubelle)
end ;

procedure insert(p : PointeurCellule ; var s : Liste) ;
begin
  if (s = nil) or (s^.info > p^.info) then
   begin
     p^.svt := s ; s := p
   end
  else insert(p,s^.svt)
end ;
        
function isEmpty(s : liste) : Boolean ;
begin
  Result := s = nil
end ;

procedure addTmp(var L : Liste) ;
var n : PointeurCellule ;
begin
  new(n) ; n^.svt := L ; L := n
end ;

procedure delTmp(var L : Liste) ;
var n : PointeurCellule ;
begin
  n := L ; L := L^.svt ; dispose(n)
end ;

{
function delete(var L : Liste) : Liste ; 
var p,c : PointeurCellule ;
begin
  create(Result) ;
  if L <> nil then
  begin
    addTmp(L) ;
    //--------
    p := L ; c := L^.svt ;
    while c <> nil do
      if c^.info MOD 2 = 0 then
        begin
          p^.svt := c^.svt ;
          insert(c,Result) ;
          c := p^.svt
        end
      else
        begin
          p := c ;
          c := c^.svt
        end
  
    //--------
    delTmp(L)
  end
end ;


function delete(var L : Liste) : Liste ; 
  procedure del(var L : Liste) ;
  var p : PointeurCellule ;
  begin
   if L <> nil then 
     if L^.info MOD 2 = 0 then
        begin
          p := L ;
          L := L^.svt ;
          insert(p,Result) ;
          del(L)
        end
      else del(L^.svt)
  end ;
begin
  create(Result) ;
  del(L)
end ;
}


function delete(var L : Liste) : Liste ; 
var p : Liste ;
begin
  if L = nil then Result := nil
  else
    begin
      Result := delete(L^.svt) ;
      if L^.info MOD 2 = 0 then
        begin
          p := L ;
          L := L^.svt ;
          insert(p,Result)
        end
    end
end ;

procedure afficher(s : Liste) ;
begin
  while s <> nil do
  begin
    write(s^.info : 4) ;
    s := s^.svt
  end ;
  writeLn
end ;
   
end.




