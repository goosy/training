program exa1 ;
uses SysUtils ;

Const	LGMAX = 10 ;
Type	TData = record
	  Clef : String ;  	//pour simplifier
          num : Cardinal 
        end{TData} ;

        Tableau = record 
          tab : array[1..LGMAX] of TData ;
          longueur : 0..LGMAX
        end{Tableau} ;
                
procedure pack(var T : Tableau) ;
var r,w : Cardinal ;
begin
  with T do
  if longueur > 1 then
  begin
    w := 1 ;
    for r := 2 to longueur do
      if tab[r].clef = tab[w].clef then
        inc(tab[w].num, tab[r].num)
      else
        begin
          inc(w) ;
          tab[w] := tab[r]
        end
     {fi}
   {od} ;
    longueur := w
  end
end ;

//-----------------------------------------------
function data(c : String; n : Cardinal) : TData ;
begin
  Result.clef := c ; Result.num := n
end ;

function toString(d : TData) : String ;
begin
  Result := '['+d.clef+' '+intToStr(d.num)+']' 
end ;

procedure init(out T : Tableau) ;
begin
  T.longueur := 0
end ;

procedure add(d : TData ; var T : Tableau) ;
begin
  inc(T.longueur) ;
  T.tab[T.longueur] := d
end ;

procedure print(const T : Tableau) ;
var k : Cardinal ;
begin
  for k := 1 to T.longueur do
    write(toString(T.tab[k])) ;
  writeLn
end ;
//-----------------------------------------------

var T : Tableau ;
begin
  init(T) ;
  add(data('A',3),T) ;add(data('A',2),T) ;add(data('A',1),T) ;
  add(data('C',7),T) ;
  add(data('F',2),T) ;add(data('F',6),T) ;
  print(T) ;
  
  pack(T) ;
  print(T) ;
  
end.
