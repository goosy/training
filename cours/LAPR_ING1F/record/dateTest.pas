program dateTest ;
uses UDate ;
var d1, d2 : Date ;
begin
  lire(d1) ;
  lire(d2) ;
  writeln('------------------------------') ;
  afficher(d1) ;
  afficher(d2) ;
  writeln('------------------------------') ;
  writeln(compare(d1,d2))
end.