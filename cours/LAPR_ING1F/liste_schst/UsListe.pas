unit UsListe;

interface
type
      Element = char ;      { par exemple ! }
      PointeurCellule = ^Cellule ;
      Cellule =  record
                   info : Element ;
                   svt  : PointeurCellule
                 end ;
      Liste = PointeurCellule ;


 { d�s l'initialisation, toute op�ration sur les listes doit les maintenir tri�es. avec unicit� des �l�ments }


   procedure creerListe(out s : Liste) ;
   { effet : initialise s � une liste vide }

   procedure inserer(x : Element ; var s : Liste) ;
   { effet : ins�re l'�l�ment x dans la liste s ; 
             ne fait rien si dansListe(x,s)
     post : dansListe(x,s) }

   procedure supprimer(x: Element ; var s : Liste) ;
   { effet : supprime l'�l�ment x de la liste s ;  
       ne fait rien si not dansListe(x,s)
     post : not dansListe(x,s) }

   procedure copier(out Lout : Liste ; Lin : Liste) ; overload ;
   { effet : copie profonde de la liste Lin dans la 
		       liste Lout }

   function copier(L : Liste) : Liste ; overload ;
   { renvoie : une liste, copie profonde de la liste L. }

    
   function listeVide(s : liste) : Boolean ;
   { renvoie : True si la liste s est vide, False sinon }

   function dansListe(x : Element ; s : Liste): Boolean ;
   { renvoie : True si l'�l�ment x est dans la liste s,                
           False sinon }
 
   function egales(s1 , s2 : Liste) : Boolean ;
   { renvoie : True si les listes s1 et s2 sont des copies  
               profondes l'une de l'autre, False  sinon}


   procedure afficher(s : Liste) ;
   { effet : affiche (en ordre croissant) les �l�ments de 
             la liste s, puis va � la ligne }


implementation

// ... A faire ...

end.




