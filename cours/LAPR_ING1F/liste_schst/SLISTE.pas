program SLISTE;

uses UsListe in 'UsListe.pas';

function create(s : String) : Liste ;
var k : Cardinal ;
begin
  creerListe(Result) ;
  for k := 1 to length(s) do inserer(s[k],Result) ;
end ;


var  L : Liste ; 
begin
   L := create('AZERTY') ;
   afficher(L) ;                    // A  E  R  T  Y  Z
   supprimer('Z',L) ; supprimer('Y',L) ;
   afficher(L) ;                    // A  E  R  T 

end .
