unit UListe;
{ version r�cursive }

interface
type
      Element = char ;      { par exemple ! }
      PointeurCellule = ^Cellule ;
      Cellule =  record
                   info : Element ;
                   svt  : PointeurCellule
                 end ;
      Liste = PointeurCellule ;


 { d�s l'initialisation, toute op�ration sur les listes doit les maintenir tri�es. avec unicit� des �l�ments }


   procedure creerListe(out s : Liste) ;
   { effet : initialise s � une liste vide }

   procedure inserer(x : Element ; var s : Liste) ;
   { effet : ins�re l'�l�ment x dans la liste s ; 
             ne fait rien si dansListe(x,s)
     post : dansListe(x,s) }

   procedure supprimer(x: Element ; var s : Liste) ;
   { effet : supprime l'�l�ment x de la liste s ;  
       ne fait rien si not dansListe(x,s)
     post : not dansListe(x,s) }

   procedure copier(out Lou : Liste ; Lin : Liste) ; overload ;
   { effet : copie profonde de la liste Lin dans la 
		       liste Lout }

   function copier(L : Liste) : Liste ; overload ;
   { renvoie : une liste, copie profonde de la liste L. }

    
   function listeVide(s : liste) : Boolean ;
   { renvoie : True si la liste s est vide, False sinon }

   function dansListe(x : Element ; s : Liste): Boolean ;
   { renvoie : True si l'�l�ment x est dans la liste s,                
           False sinon }
 
   function egales(s1 , s2 : Liste) : Boolean ;
   { renvoie : True si les listes s1 et s2 sont des copies  
               profondes l'une de l'autre, False  sinon}


   procedure afficher(s : Liste) ;
   { effet : affiche (en ordre croissant) les �l�ments de 
             la liste s, puis va � la ligne }

   
implementation

procedure creerListe(out s : Liste) ;
begin
  s := nil 
end ;

procedure addFirst(x : Element ; var L : Liste) ; 
var nv : Liste ;
begin
  new(nv) ; nv^.info := x ; nv^.svt := L ; L := nv
end ;

function enteter(x : Element; L : Liste) : Liste ; 
begin
  new(Result) ;
  Result^.info := x ;
  Result^.svt := L
end ;

procedure delFirst(var L : Liste) ;
var poubelle : PointeurCellule ;
begin
  poubelle := L ; L := L^.svt ; dispose(poubelle)
end ;

procedure inserer(x : Element ; var s : Liste) ;
begin
  if (s = nil) or(s^.info > x) then addFirst(x,s)
  else if s^.info < x then inserer(x,s^.svt)
end ;
  
procedure supprimer(x: Element ; var s : Liste) ;
begin
  if (s <> nil) and (s^.info <= x) then
  begin
    if s^.info = x then delFirst(s)
    else supprimer(x,s^.svt)
  end
end ;

procedure copier(out Lou : Liste ; Lin : Liste) ; 
begin
  creerListe(Lou) ;
  if Lin <> nil then
    begin
      addFirst(Lin^.info,Lou) ;
      copier(Lou^.svt,Lin^.svt)
    end
end ;
   
function copier(L : Liste) : Liste ;
begin
  if L = nil then Result := nil
  else Result := enteter(L^.info,copier(L^.svt))
end ;

function listeVide(s : liste) : Boolean ;
begin
  Result := s = nil
end ;
   
function dansListe(x : Element ; s : Liste): Boolean ;
begin
  if (s = nil) or (s^.info > x) then Result := false
  else if s^.info < x then Result := dansListe(x,s^.svt)
  else { s^.info = x } Result := true
end ;
   
function egales(s1 , s2 : Liste) : Boolean ;
begin
  if (s1 = nil) and (s2 = nil) then Result := true
  else if (s1 = nil) xor (s2 = nil) then Result := false
  else Result := (s1^.info = s2^.info) and egales(s1^.svt,s2^.svt)
end ;
   
procedure afficher(s : Liste) ;
begin
  if  s = nil then writeln
  else
    begin
      write(s^.info : 4) ;
      afficher(s^.svt)
    end 
end ;
   
end.




