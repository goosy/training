program deTest ;

uses UListe ;

function create(s : String) : Liste ;
var k : Cardinal ;
begin
  creerListe(Result) ;
  for k := 1 to length(s) do inserer(s[k],Result) ;
end ;


var  L,M : Liste ; 
begin
   L := create('AAAZERTY') ;
   afficher(L) ; // A  E  R  T  Y  Z
   copier(M,L) ;
   supprimer('Y',L) ;
   afficher(L) ;
   afficher(M) ;
   writeLn(egales(L,M))
end .
