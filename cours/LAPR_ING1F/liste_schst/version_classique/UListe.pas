unit UListe;
{ version classique }

interface
type
      Element = char ;      { par exemple ! }
      PointeurCellule = ^Cellule ;
      Cellule =  record
                   info : Element ;
                   svt  : PointeurCellule
                 end ;
      Liste = PointeurCellule ;


 { d�s l'initialisation, toute op�ration sur les listes doit les maintenir tri�es. avec unicit� des �l�ments }


   procedure creerListe(out s : Liste) ;
   { effet : initialise s � une liste vide }

   procedure inserer(x : Element ; var s : Liste) ;
   { effet : ins�re l'�l�ment x dans la liste s ; 
             ne fait rien si dansListe(x,s)
     post : dansListe(x,s) }

   procedure supprimer(x: Element ; var s : Liste) ;
   { effet : supprime l'�l�ment x de la liste s ;  
       ne fait rien si not dansListe(x,s)
     post : not dansListe(x,s) }

   procedure copier(out Lout : Liste ; Lin : Liste) ; overload ;
   { effet : copie profonde de la liste Lin dans la 
		       liste Lout }

   function copier(L : Liste) : Liste ; overload ;
   { renvoie : une liste, copie profonde de la liste L. }

    
   function listeVide(s : liste) : Boolean ;
   { renvoie : True si la liste s est vide, False sinon }

   function dansListe(x : Element ; s : Liste): Boolean ;
   { renvoie : True si l'�l�ment x est dans la liste s,                
           False sinon }
 
   function egales(s1 , s2 : Liste) : Boolean ;
   { renvoie : True si les listes s1 et s2 sont des copies  
               profondes l'une de l'autre, False  sinon}


   procedure afficher(s : Liste) ;
   { effet : affiche (en ordre croissant) les �l�ments de 
             la liste s, puis va � la ligne }

   
implementation

procedure creerListe(out s : Liste) ;
begin
  s := nil 
end ;

procedure addFirst(x : Element ; var L : Liste) ; 
var nv : Liste ;
begin
  new(nv) ; nv^.info := x ; nv^.svt := L ; L := nv
end ;

procedure delFirst(var L : Liste) ;
var poubelle : PointeurCellule ;
begin
  poubelle := L ; L := L^.svt ; dispose(poubelle)
end ;

procedure inserer(x : Element ; var s : Liste) ;
var prec , cour : PointeurCellule ;
begin
  if (s = nil) or (s^.info > x) then addFirst(x,s)
  else if s^.info < x then
   begin
     prec := s ; cour := s^.svt ;
     while (cour <> nil) and (cour^.info < x) do
     begin
       prec := cour ;
       cour := cour^.svt 
     end ;
     // (cour = nil) or (cour^.info >= x)
     if (cour = nil) or (cour^.info > x) then
         addFirst(x,prec^.svt)    
   end
end ;

procedure supprimer(x: Element ; var s : Liste) ;
var prec , cour : PointeurCellule ;
begin
  if (s <> nil) and (s^.info <= x) then  
    if s^.info = x then delFirst(s)
    else
      begin
        prec := s ; cour := s^.svt ;
	while (cour <> nil) and (cour^.info < x) do
	 begin
	  prec := cour ;
	  cour := cour^.svt 
	 end ;
        // (cour = nil) or (cour^.info >= x)
        if (cour <> nil) and (cour^.info = x) then
          delFirst(prec^.svt)
      end  
end ;
   
function copier(L : Liste) : Liste ; overload ;
var p : PointeurCellule ;
begin
  if L = nil then Result := nil 
  else
   begin
     new(Result) ; Result^.info := L^.info ; 
     L := L^.svt ; p := Result ;
     while L <> nil do
     begin
       new(p^.svt) ; p := p^.svt ;
       p^.info := L^.info ; L := L^.svt   
     end ;
     p^.svt := nil
   end
end ;

procedure copier(out Lout : Liste ; Lin : Liste) ;
begin
  Lout := copier(Lin)
end ;
       
function listeVide(s : liste) : Boolean ;
begin
  Result := s = nil
end ;
   
function dansListe(x : Element ; s : Liste): Boolean ;
begin
  while (s <> nil) and (s^.info < x) do s := s^.svt ;
  // (s = nil) or (s^.info >= x)
  Result := (s <> nil) and (s^.info = x)
end ;
   
function egales(s1 , s2 : Liste) : Boolean ;
begin
  while (s1 <> nil) and (s2 <> nil) and (s1^.info = s2^.info) do
    begin
      s1 := s1^.svt ;
      s2 := s2^.svt
    end ;
  Result := (s1 = nil) and (s2 = nil)
end ;
   
procedure afficher(s : Liste) ;
begin
  while s <> nil do
  begin
    write(s^.info : 4) ;
    s := s^.svt
  end ;
  writeLn
end ;
   
end.




