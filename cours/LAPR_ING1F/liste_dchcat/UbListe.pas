unit UbListe;

interface
type
      Element = char ;      { par exemple ! }
      PointeurCellule = ^Cellule ;
      Cellule =  record
                   info : Element ;
                   prec , svt : PointeurCellule
                 end ;
      Liste = PointeurCellule ;


 { d�s l'initialisation, toute op�ration sur les listes   doit les maintenir tri�es. }


   procedure creerListe(out s : Liste) ;
   { effet : initialise s � une liste vide }

   procedure inserer(x : Element ; var s : Liste) ;
   { effet : ins�re l'�l�ment x dans la liste s ; 
             ne fait rien si dansListe(x,s)
     post : dansListe(x,s) }

   procedure supprimer(x: Element ; var s : Liste) ;
   { effet : supprime l'�l�ment x de la liste s ;  
       ne fait rien si not dansListe(x,s)
     post : not dansListe(x,s) }

   procedure copier(out Lout : Liste ; Lin : Liste) ; overload ;
   { effet : copie profonde de la liste Lin dans la 
		       liste Lout }

   function copier(L : Liste) : Liste ; overload ;
   { renvoie : une liste, copie profonde de la liste L. }

    
   function listeVide(s : liste) : Boolean ;
   { renvoie : True si la liste s est vide, False sinon }

   function dansListe(x : Element ; s : Liste): Boolean ;
   { renvoie : True si l'�l�ment x est dans la liste s,                
           False sinon }
 
   function egales(s1 , s2 : Liste) : Boolean ;
   { renvoie : True si les listes s1 et s2 sont des copies  
               profondes l'une de l'autre, False  sinon}


   procedure afficher(s : Liste) ;
   { effet : affiche (en ordre croissant) les �l�ments de 
             la liste s, puis va � la ligne }

   procedure rehciffa(s : Liste) ;
   { effet : affiche (en ordre d�croissant) les �l�ments de 
             la liste s, puis va � la ligne }
             
   function fusion(s1, s2 : Liste) : Liste ;
   { renvoie : la liste fusion des liste s1 et s2 }

implementation

procedure creerListe(out s : Liste) ;
begin
      new(s) ; s^.prec := s ; s^.svt := s
end ;

function listeVide(s : liste) : Boolean ;
begin
      Result := (s^.prec = s) and (s^.svt = s)
end ;

function dansListe(x : Element ; s : Liste):Boolean ;
var courant : pointeurCellule ;
begin
  courant := s^.svt ;
  while (courant <> s) and (courant^.info < x) do
	courant := courant^.svt ;
//assert((courant = s)or(courant^.info >= x)) ;
  result := (courant <> s) and (courant^.info = x)
end ;


procedure ajouterAvant(x:Element;var cour: PointeurCellule);
var nv : PointeurCellule ;
begin
       new(nv) ;
       nv^.info := x ;
       nv^.svt := cour ;
       nv^.prec := cour^.prec ;
       cour^.prec := nv ;
       nv^.prec^.svt := nv
end ;

procedure inserer(x : Element ; var s : Liste) ;
var courant : PointeurCellule ;
begin
  courant := s^.svt ;
  while (courant <> s) and (courant^.info < x) do
	courant := courant^.svt ;
//assert((courant = s)or(courant^.info >= x)) ;
  if (courant = s) or (courant^.info > x) then 
	ajouterAvant(x,courant)
end ;

procedure supprimerCourant(cour : PointeurCellule) ;
begin
        cour^.prec^.svt := cour^.svt ;
        cour^.svt^.prec := cour^.prec ;
        dispose(cour)
end ;

procedure supprimer(x: Element ; var s : Liste) ;
var courant : PointeurCellule ;
begin
  courant := s^.svt ;
  while (courant <> s) and (courant^.info < x) do
	courant := courant^.svt ;
//assert((courant = s)or(courant^.info >= x)) ;
  if (courant <> s) and (courant^.info = x) then
         supprimerCourant(courant)
end ;

procedure afficher(s : Liste) ;
var cour : PointeurCellule ;
begin
       cour := s^.svt ;
       while cour <> s do
         begin
           write(cour^.info:4) ;
           cour := cour^.svt
         end ;
       writeln
end ;



procedure rehciffa(s : Liste) ;
var cour : PointeurCellule ;
begin
       cour := s^.prec ;
       while cour <> s do
         begin
           write(cour^.info:4) ;
           cour := cour^.prec
         end ;
       writeln
end ;


procedure copier(out Lout : Liste ; Lin : Liste) ;
begin
          { � faire comme exercice }
end ;

function copier(L : Liste) : Liste ;
begin
	  { � faire comme exercice }
end ;

function egales(s1 , s2 : Liste) : Boolean ;
begin
          { � faire comme exercice }
end ;

function fusion(s1, s2 : Liste) : Liste ;
begin
	  { � faire comme exercice }
end ;

end.




