program BLISTE;

uses UbListe in 'UbListe.pas';

function create(s : String) : Liste ;
var k : Cardinal ;
begin
  creerListe(Result) ;
  for k := 1 to length(s) do inserer(s[k],Result) ;
end ;


var  L : Liste ; 
begin
   L := create('AZERTY') ;
   afficher(L) ;                    // A  E  R  T  Y  Z
   supprimer('Z',L) ; supprimer('Y',L) ;
   rehciffa(L) ;                    // T  R  E  A

end .
