program testris ;

uses chapitre10 ;

type Tableau = array[1..10] of Integer ;

const TAB : Tableau = (19,87,23,12,78,87,98,25,36,71) ;

var T1,T2,T3 : Tableau ;

begin
	T1 := TAB ;
	afficher(T1) ;
	tri_Selection(T1) ;
	afficher(T1) ;
	
	T2 := TAB ;
	afficher(T2) ;
	tri_Insertion(T2) ;
	afficher(T2) ;
	
	T3 := TAB ;
	afficher(T3) ;
	tri_Bulles(T3) ;
	afficher(T3) ;
	
	readLn
	
end.	