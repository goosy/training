program TestTableau ;

uses SysUtils, UTableau ;

procedure insert(x : Item ; var T : tableau ; pos : Cardinal) ;
var k : cardinal ;
begin
  {add(get(T,size(T)),T) ;
  for k := size(T)-1 downto pos+1 do replace(get(T,k-1),T,k) ;
  replace(x,T,pos)}
  for k := pos to size(T) do x := replace(x,T,k) ;
  add(x,T)
end ;

function delete(var T : Tableau ; pos : Cardinal) : Item ;
var k : Cardinal ; 
begin
  Result := pop(T) ;
  for k := size(T) downto pos do Result := replace(Result,T,k) ;
end ;

var  T : Tableau ;
     k : Cardinal ;

begin
  init(T) ;
  randomize ;
  for k := 1 to 10 do add(random(100),T) ;
  writeLn(toString(T)) ;
  insert(111,T,3) ;
  writeLn(toString(T)) ;
  delete(T,1) ;
  writeLn(toString(T)) ;
  
end.