unit UTableau ;

interface
  uses SysUtils ;

  const N = 20 ;
  type	Item = Integer ;
  	Tableau = record
  	  tab : array [1..N] of Item ;
  	  dim : 0..N ;
  	end{Tableau} ;
  	
  	ETableau = class(Exception) ;
  	EFull	 = class(ETableau) ;
  	EPos     = class(ETableau) ;
  	EEmpty   = class(ETableau) ;
  	
  procedure init(var T : Tableau) ;
  function empty(const T : Tableau) : Boolean ;
  function size(const T : tableau) : Cardinal ;
  function capacity(const T : Tableau) : Cardinal ;
  function full(const T : Tableau) : Boolean ;
  procedure add(valeur : Item; var T : tableau) ;
  procedure addIf(valeur : Item; var T : tableau) ;
  function get(const T : tableau ; pos : Cardinal) : Item ;
  function replace(valeur : Item ; var T : tableau ; pos : Cardinal) : Item ;
  function pop(var T : Tableau) : Item ;
  function toString(const T : Tableau) : String ;
  
implementation

  procedure init(var T : Tableau) ;
  begin
    T.dim := 0 
  end ;
  
  function empty(const T : Tableau) : Boolean ;
  begin
    Result := T.dim = 0 
  end ;
  
  function size(const T : tableau) : Cardinal ;
  begin
    Result := T.dim
  end ;
  
  function capacity(const T : Tableau) : Cardinal ;
  begin
    Result := N 
  end ;
  
  function full(const T : Tableau) : Boolean ;
  begin
    Result := T.dim = N
  end ;
  
  procedure add(valeur : Item; var T : tableau) ;
  begin
    if full(T) then raise EFull.create('le tableau est plein!!!') ;
    inc(T.dim) ;
    T.tab[T.dim] := valeur
  end ;
  
  procedure addIf(valeur : Item; var T : tableau) ;
    begin
      if not full(T) then 
        begin
          inc(T.dim) ;
          T.tab[T.dim] := valeur
        end  
  end ;
  
  function toString(const T : Tableau) : String ;
  var k : Cardinal ;
  begin
    Result := '' ;
    for k := 1 to T.dim do Result := Result + intToStr(T.tab[k]) + '  ' ;
  end ;
  
  procedure checkIndex(const T : Tableau ; indice : Cardinal) ;
  begin
    if (indice = 0) or (indice > T.dim) then
      raise EPos.create('position non valable!!!') 
  end ;
  
  function get(const T : tableau ; pos : Cardinal) : Item ;
  begin
    checkIndex(T,pos) ;
    Result := T.tab[pos]
  end ;
  
  function replace(valeur : Item ; var T : tableau ; pos : Cardinal) : Item ;
  begin
    checkIndex(T,pos) ;
    Result := T.tab[pos] ;
    T.tab[pos] := valeur
  end ;
  
  function pop(var T : Tableau) : Item ;
  begin
    if empty(T) then raise EEmpty.create('le tableau est vide!!!') ;
    Result := T.tab[T.dim] ;
    dec(T.dim)
  end ;
  
  
  
end.  