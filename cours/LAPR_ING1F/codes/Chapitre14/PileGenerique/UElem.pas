unit UElem ;

interface

uses SysUtils ;

type	Element = record
			clef : Integer ;
			data : String 
	end{Element} ;
	
	procedure creer(out x : Element ; clef : Integer ; data : String) ;
	procedure setClef(var x : Element ; clef : Integer) ;
	procedure setData(var x : Element ; data : String) ;
	function getClef(x : Element) : Integer ;
	function getData(x : Element) : String ;
	
	procedure copier(out x : Element ; y : Element) ;
	function egal(x , y : Element) : Boolean ;
	
	function toString(x : Element) : String ;

implementation


	procedure creer(out x : Element ; clef : Integer ; data : String) ;
	begin
		x.clef := clef ;
		x.data := data 
	end ;
	
	procedure setClef(var x : Element ; clef : Integer) ;
	begin
		x.clef := clef 
	end ;
	
	procedure setData(var x : Element ; data : String) ;
	begin
		x.data := data 
	end ;
	
	function getClef(x : Element) : Integer ;
	begin
		Result := x.clef 
	end ;
	
	function getData(x : Element) : String ;
	begin
		Result := x.data
	end ;
		
	procedure copier(out x : Element ; y : Element) ;
	begin
		x := y
	end ;
	
	function egal(x , y : Element) : Boolean ;
	begin
		Result := x.clef = y.clef
	end ;
	
	function toString(x : Element) : String ;
	begin
	   	Result := intToStr(x.clef) + '   ' + x.data ;
	end ;
	
end.	