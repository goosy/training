program PPile;

uses   UPileDyn , UElem ;

var S : Stack ;
    x,y,z : Element ;
    
begin
    creerPile(S) ;
    
    UElem.creer(x,1,'AAA') ;
    empiler(x,S) ;
    UElem.creer(y,2,'BBB') ;
    empiler(y,S) ;
    UElem.creer(z,3,'CCC') ;
    empiler(z,S) ;
    
    while not pileVide(S) do
    begin
         writeLn(UElem.toString(sommet(S))) ;
         depiler(S)
    end ;
    writeLn ;

end .
