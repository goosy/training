//UComparable.pas
//Pirlot Philippe 
//18/03/2002
unit UComparable ;

interface
	type Comparable = char ; // pour simplifier

	procedure copy(out x : Comparable ; y : Comparable) ;
	function equals(x , y : Comparable ) : Boolean ;
	function toString(x : Comparable) : String ;
	function compareTo(x,y : Comparable) : Integer ;
	procedure swap(var x,y : Comparable) ;
	function constructs : Comparable ;
	
implementation

	function constructs : Comparable ;
	begin
		Result := Comparable(ord('A')+random(26))
	end ;
	
	procedure copy(out x : Comparable ; y : Comparable) ;
	begin
		x := y
	end ;
	
	function equals(x , y : Comparable ) : Boolean ;
	begin
		Result := x = y
	end ;
	
	function toString(x : Comparable) : String ;
	begin
		Result := x
	end ;
	
	function compareTo(x,y : Comparable) : Integer ;
	begin
		Result := ord(x) - ord(y)
	end ;
	
	procedure swap(var x,y : Comparable) ;
	var tmp : Comparable ;
	begin
		tmp := x ; x := y ; y := tmp 
	end ;
	
initialization
	randomize ;
end.