//USort.pas
//Pirlot Philippe 
//18/03/2002
unit USort ;

interface
uses UComparable ;

	procedure qsort(var T : array of Comparable) ;
	
	
implementation	

  procedure qsort(var T : array of Comparable) ;

   procedure pivoter(var i,j : Integer) ;
   { pr� : i < j }
   var pivot : Comparable ;
   begin
        copy(pivot,T[(i+j) div 2 ]) ;
        repeat
           while compareTo(T[i],pivot) < 0 do inc(i) ;
           while compareTo(T[j],pivot) > 0 do dec(j) ;
           if i <= j then
             begin
                swap(T[i],T[j]) ;
                inc(i) ;
                dec(j)
             end
        until i > j
   end ;

   procedure sort(g,d : Integer) ;
   { pr� : g < d }
   var i,j : Integer ;
   begin
        i := g ;
        j := d ;
        pivoter(i,j) ;
        if g < j then sort(g,j) ;
        if i < d then sort(i,d) ;
   end ;

begin
     if low(T) < high(T) then sort(low(T),high(T)) ;
end ;


end .
