program testQSort ;
uses UComparable, USort ;

const N = 10 ;
type Tableau = array[1..N] of Comparable ;

procedure afficher(const T : array of Comparable) ;
var k : Cardinal ;
begin
     for k := low(T) to high(T) do write(UComparable.toString(T[k]):4) ;
     writeLn
end ;

procedure generer(var T : array of Comparable) ;
var k : cardinal ;
begin
     for k := low(T) to high(T) do T[k] := UComparable.constructs ;
end ;

var T : Tableau ;
begin
	generer(T) ;
	afficher(T) ;
	qsort(T) ;
	afficher(T) ;
	
end.
