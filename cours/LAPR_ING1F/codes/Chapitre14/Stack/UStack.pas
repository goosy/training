unit UStack;

interface

uses SysUtils , UItem ;

type   
        Stack = ^Cellule ;
        Cellule = record
                        info : Item ;
                        pred : Stack
                  end ;

	EStackException = class(Exception) ;
        EStackUnderflow = class(EStackException) ;
        EStackOverflow  = class(EStackException) ;

   function emptyStack : Stack ;
   function isEmpty(const s : Stack) : Boolean ;
   function isFull(const S : Stack) : Boolean ;
   procedure push(x : Item ; var s : Stack) ;
   procedure pop(var s : Stack) ;
   function peek(const s : Stack) : Item ;

implementation

   function emptyStack : Stack ;
   begin
        Result := nil
   end ;

   function isEmpty(const s : Stack) : Boolean ;
   begin
        Result := s = nil
   end ;

   function isFull(const s : Stack) : Boolean ;
   begin
        Result := False
   end ;

   procedure push(x : Item ; var s : Stack) ;
   var nv : Stack ;
   begin
        new(nv) ;
        nv^.info := UItem.copyFrom(x) ;
        nv^.pred := s ;
        s := nv
   end ;

   procedure pop(var s : Stack) ;
   var poubelle : Stack ;
   begin
     if s = nil then 
        raise EStackUnderflow.Create('la pile est vide...');
     poubelle := s ;
     s := s^.pred ;
     dispose(poubelle)
   end ;

   function peek(const s : Stack) : Item ;
   begin
     if s = nil then 
        raise EStackUnderflow.Create('la pile est vide...');
     Result := UItem.copyFrom(s^.info)
   end ;

end.
