program StackTest ;

uses   UStack ;

var S : Stack ;
    k : Item ;
    
begin
    S := emptyStack ;
    try
      pop(S) ;
    except
      on e : EStackUnderflow do writeLn(e.message) ;
    end ;
    for k := 'A' to 'F' do push(k,S) ;
    while not isEmpty(S) do
    begin
         write(peek(S):4) ;
         pop(S)
    end ;
    writeLn ;

end .
