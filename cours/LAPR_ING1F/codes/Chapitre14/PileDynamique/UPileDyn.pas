unit UPileDyn;

interface

uses SysUtils ;

type    Element = char ; // pour simplifier... 
        Stack = ^Cellule ;
        Cellule = record
                        info : Element ;
                        pred : Stack
                  end ;

		  EStackException = class(Exception) ;
        EStackUnderflow = class(EStackException) ;
        EStackOverflow  = class(EStackException) ;

   procedure creerPile(out s : Stack) ;
   function pileVide(const s : Stack) : Boolean ;
   function pilePleine(const S : Stack) : Boolean ;
   procedure empiler(x : Element ; var s : Stack) ;
   procedure depiler(var s : Stack) ;
   function sommet(const s : Stack) : Element ;

implementation

   procedure creerPile(out s : Stack) ;
   begin
        s := nil
   end ;

   function pileVide(const s : Stack) : Boolean ;
   begin
        Result := s = nil
   end ;

   function pilePleine(const S : Stack) : Boolean ;
   begin
        Result := False
   end ;

   procedure empiler(x : Element ; var s : Stack) ;
   var nv : stack ;
   begin
        new(nv) ;
        nv^.info := x ;
        nv^.pred := s ;
        s := nv
   end ;

   procedure depiler(var s : Stack) ;
   var poubelle : Stack ;
   begin
     if s = nil then 
        raise EStackUnderflow.Create('la pile est vide...');
     poubelle := s ;
     s := s^.pred ;
     dispose(poubelle)
   end ;

   function sommet(const s : Stack) : Element ;
   begin
     if s = nil then 
        raise EStackUnderflow.Create('la pile est vide...');
     Result := s^.info
   end ;

end.
