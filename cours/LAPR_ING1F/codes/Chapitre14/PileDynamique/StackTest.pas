program PPile;

uses   UPileDyn ;

var S : Stack ;
    k : Element ;
    
begin
    creerPile(S) ;
    try
      depiler(S) ;
    except
      on e : EStackUnderflow do writeLn(e.message) ;
    end ;
    for k := 'A' to 'F' do empiler(k,S) ;
    while not pileVide(S) do
    begin
         write(sommet(S):4) ;
         depiler(S)
    end ;
    writeLn ;

end .
