unit UPileStat;

interface

uses SysUtils ;

const   TAILLEMAX = 10 ;  	// par exemple...
type    Element = char ; 	// pour simplifier... 
        Stack = record
             indsommet : 0..TAILLEMAX ;
             tab : array[1..TAILLEMAX] of Element ;
        end{Stack} ;

        EStackException = class(Exception) ;
        EStackUnderflow = class(EStackException) ;
        EStackOverflow  = class(EStackException) ;

   procedure creerPile(out s : Stack) ;
   function pileVide(const s : Stack) : Boolean ;
   function pilePleine(const S : Stack) : Boolean ;
   procedure empiler(x : Element ; var s : Stack) ;
   procedure depiler(var s : Stack) ;
   function sommet(const s : Stack) : Element ;



implementation

   
   procedure creerPile(out s : Stack) ;
   begin
       S.indSommet := 0
   end ;

   function pileVide(const s : Stack) : Boolean ;
   begin
        Result := S.indSommet = 0
   end ;

   function pilePleine(const S : Stack) : Boolean ;
   begin
        Result := S.indSommet = TAILLEMAX
   end ;

   procedure empiler(x : Element ; var S : Stack) ;
   begin
        if pilePleine(S) then
          raise EStackOverflow.Create('la pile est pleine...') ;
        inc(S.indSommet) ;
        S.tab[S.indSommet] := x
   end ;

   procedure depiler(var s : Stack) ;
   begin
        if pileVide(S) then
          raise EStackUnderflow.Create('la pile est vide...') ;
        dec(S.indSommet)
   end ;

   function sommet(const s : Stack) : Element ;
   begin
        if pileVide(S) then
          raise EStackUnderflow.Create('la pile est vide...') ;
        Result :=  S.tab[S.indSommet]
   end ;

end.
