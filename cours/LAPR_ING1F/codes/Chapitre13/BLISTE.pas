program BLISTE;
// {$APPTYPE CONSOLE}
uses UbListe in 'UbListe.pas';

const str : String = 'AZERTY' ;
var  L : Liste ; k : Cardinal ;
begin
   creerListe(L) ;
   for k := 1 to length(str) do inserer(str[k],L) ;
   afficher(L) ;                    // A  E  R  T  Y  Z
   supprimer('Z',L) ; supprimer('Y',L) ;
   rehciffa(L) ;                    // T  R  E  A

   // readLn
end .
