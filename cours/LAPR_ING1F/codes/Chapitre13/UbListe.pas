unit UbListe;

interface
type
      Element = char ;      { par exemple ! }
      PointeurCellule = ^Cellule ;
      Cellule =  record
                   info : Element ;
                   prec , svt : PointeurCellule
                 end ;
      Liste = PointeurCellule ;

{ -- INVARIANTS - 
 d�s l'initialisation, toute op�ration sur les listes   doit les maintenir tri�es. }

{ -- CONSTRUCTEURS -- }
   procedure creerListe(var s : Liste) ;
   { effet : initialise s � une liste vide }

   procedure inserer(x : Element ; var s : Liste) ;
   { effet : ins�re l'�l�ment x dans la liste s ; 
             ne fait rien si dansListe(x,s)
     post : dansListe(x,s) }

   procedure supprimer(x: Element ; var s : Liste) ;
   { effet : supprime l'�l�ment x de la liste s ;  
       ne fait rien si not dansListe(x,s)
     post : not dansListe(x,s) }

   procedure copier(var Lout : Liste ; Lin : Liste) ;
   { effet : copie profonde de la liste Lin dans la 
		       liste Lout }

{ -- SELECTEURS -- }
    
   function listeVide(s : liste) : Boolean ;
   { renvoie : True si la liste s est vide, False sinon }

   function dansListe(x : Element ; s : Liste): Boolean ;
   { renvoie : True si l'�l�ment x est dans la liste s,                
           False sinon }
 
   function egales(s1 , s2 : Liste) : Boolean ;
   { renvoie : True si les listes s1 et s2 sont des copies  
               profondes l'une de l'autre, False  sinon}

{ -- ITERATEURS -- }
   procedure afficher(s : Liste) ;
   { effet : affiche (en ordre croissant) les �l�ments de 
             la liste s, puis va � la ligne }

   procedure rehciffa(s : Liste) ;
   { effet : affiche (en ordre d�croissant) les �l�ments de 
             la liste s, puis va � la ligne }

implementation

procedure creerListe(var s : Liste) ;
begin
      new(s) ; s^.prec := s ; s^.svt := s
end ;

function listeVide(s : liste) : Boolean ;
begin
      Result := (s^.prec = s) and (s^.svt = s)
end ;

procedure jusqua(x:Element;var P: PointeurCellule);
var s : Liste ;
begin
       s := P ;
       P := P^.svt ;
       while (P <> s) and (P^.info < x) do P := P^.svt
end ;

function dansListe(x : Element ; s : Liste):Boolean ;
var courant : pointeurCellule ;
begin
       courant := s ;
       jusqua(x,courant) ;
       assert((courant = s) or (courant^.info >= x)) ;
       result := (courant <> s) and (courant^.info = x)
end ;






procedure ajouterAvant(x:Element;var cour: PointeurCellule);
var nv : PointeurCellule ;
begin
       new(nv) ;
       nv^.info := x ;
       nv^.svt := cour ;
       nv^.prec := cour^.prec ;
       cour^.prec := nv ;
       nv^.prec^.svt := nv
end ;

procedure inserer(x : Element ; var s : Liste) ;
var cour : PointeurCellule ;
begin
       cour := s ;
       jusqua(x,cour) ;
       assert((cour = s) or (cour^.info >= x)) ;
      if (cour = s) or (cour^.info > x) then 
		ajouterAvant(x,cour)

end ;

procedure supprimerCourant(cour : PointeurCellule) ;
begin
        cour^.prec^.svt := cour^.svt ;
        cour^.svt^.prec := cour^.prec ;
        dispose(cour)
end ;

procedure supprimer(x: Element ; var s : Liste) ;
var cour : PointeurCellule ;
begin
       cour := s ;
       jusqua(x,cour) ;
       assert((cour = s) or (cour^.info >= x)) ;
       if (cour <> s) and (cour^.info = x) then
         supprimerCourant(cour)
end ;

procedure afficher(s : Liste) ;
var cour : PointeurCellule ;
begin
       cour := s^.svt ;
       while cour <> s do
         begin
           write(cour^.info:4) ;
           cour := cour^.svt
         end ;
       writeln
end ;






procedure rehciffa(s : Liste) ;
var cour : PointeurCellule ;
begin
       cour := s^.prec ;
       while cour <> s do
         begin
           write(cour^.info:4) ;
           cour := cour^.prec
         end ;
       writeln
end ;

procedure copier(var Lout : Liste ; Lin : Liste) ;
begin
          { � faire comme exercice }
end ;

function egales(s1 , s2 : Liste) : Boolean ;
begin
          { � faire comme exercice }
end ;


end.
