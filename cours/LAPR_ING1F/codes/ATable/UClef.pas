unit UClef;
// dans le cas o� la clef est un String...
// relation d'ordre : compareTo
interface

//------------------------
uses 	UString ;
type	TClef = String ;
//------------------------
	
   procedure copy(var c : TClef; d : TClef) ;
   function equals(c,d : TClef) : Boolean ;
   function compareTo(c,d : TClef) : Integer ;
   function toString(c : TClef) : String ;
   function clefToCardinal(c : TClef) : Cardinal ;

implementation

   procedure copy(var c : TClef; d : TClef) ;
   begin
     UString.copy(c,d)  
   end ;

   function equals(c,d : TClef) : Boolean ;
   begin
     Result := UString.equals(c,d) ;
   end ;
   
   function compareTo(c, d : TClef) : Integer ;
   begin
     if c < d then Result := -1
     else if c > d then Result := 1
     else { c = d } Result := 0
   end ;

   function toString(c : TClef) : String ;
   begin
     Result := UString.toString(c)
   end ;

   function clefToCardinal(c : TClef) : Cardinal ;
   begin
     Result := UString.clefToCardinal(c)
   end ;

end.  
