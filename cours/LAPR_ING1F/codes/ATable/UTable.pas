unit UTable;

interface

uses UAbr, UAttribut, UClef ;

type   
       Table = Abr ;

       procedure create(out T : Table) ;
       procedure add(c:TClef;a:TAttribut;var T:Table) ;
       function contains(c:TClef ; const T : Table) : Boolean;
       procedure delete(c:TClef; var T : Table) ;
       function attribut(c:TClef; const T : Table; out A : TAttribut):Boolean;
       
implementation

       procedure create(out T : Table) ;  
       begin
          UAbr.create(T)  
       end ;

       procedure add(c:TClef;a:TAttribut;var T:Table) ;
       begin
         UAbr.add(c,a,T) ;
       end ;
              
       function contains(c:TClef ; const T : Table) : Boolean; 
       begin
         Result := contains(c,T) ;
       end ;
       
       procedure delete(c:TClef; var T : Table) ;
       begin
         UAbr.delete(c,T)
       end ;
       
       function attribut(c:TClef; const T : Table; out A : TAttribut):Boolean;
       begin
         Result := UAbr.attribut(c,T,A)
       end ;
       
      
       
end.
