unit UString ;
interface
	
   procedure copy(var c : String; d : String) ;
   function equals(c,d : String) : Boolean ;
   function toString(c : String) : String ;
   function clefToCardinal(c : String) : Cardinal ;

implementation

   procedure copy(var c : String; d : String) ;
   begin
       c := d
   end ;

   function equals(c,d : String) : Boolean ;
   begin
       Result := c = d 
   end ;

   function toString(c : String) : String ;
   begin
     Result := c
   end ;

   function clefToCardinal(c : String) : Cardinal ;
   var k : Cardinal ;
   begin
     Result := 0 ;
     for k := 1 to length(c) do  
       Result := (31*Result+byte(c[k])) MOD high(Cardinal)
   end ;

end.  
