unit UAbr;
// r�alisation d'un arbre binaire de recherche
// dans le cas particulier de la construction d'une Table
interface

uses UAttribut, UClef ;

type 
     Abr = ^Noeud ;
     Noeud = record
       clef     : TClef ;
       attribut : TAttribut ;
       g,d      : Abr
     end{Noeud} ;
     
     procedure create(out A : Abr ) ;
     function  isEmpty( A : Abr ) : Boolean ;
     function  contains(c : TClef; A : Abr ):Boolean ; 
     procedure add(c : TClef; at : TAttribut ; var A : Abr) ;
     procedure delete(c : TClef; var A : Abr) ;
     function attribut(c:TClef;A:Abr;out At : TAttribut):Boolean ;
          
implementation

     procedure create(out A : Abr ) ;
     begin
          A := nil
     end ;

     function isEmpty( A : Abr ) : Boolean ;
     begin
          Result := A = nil
     end ;
     
     function  contains(c : TClef; A : Abr ):Boolean ; 
     begin
       if A = nil then Result := false
       else if UClef.compareTo(A^.clef,c) > 0 then Result := contains(c,A^.g)
       else if UClef.compareTo(A^.clef,c) < 0 then Result := contains(c,A^.d)
       else Result := true
     end ;
     
     procedure add(c : TClef ; at : TAttribut ; var A:Abr) ;
     begin
         if A = nil then
           begin
              new(A) ;
              UClef.copy(A^.clef,c) ;
              UAttribut.copy(A^.attribut,at) ;
              A^.g := nil ; A^.d := nil
           end
         else if UClef.equals(c,A^.clef) then
              UAttribut.copy(A^.attribut,at)
         else if UClef.compareTo(A^.clef,c) > 0 then add(c,at,A^.g)
                                                else add(c,at,A^.d)
     end ;
     
     procedure delete(c : TClef; var A : Abr) ;
     var poubelle : Abr ;
     begin
       // � faire...
     end ;
     
     function attribut(c:TClef;A:Abr;out at : TAttribut):Boolean ;
     begin
        if A = nil then Result := false
        else if UClef.equals(A^.clef,c) then 
             begin
               Result := true ;
               UAttribut.copy(at,A^.attribut)
             end
        else if UClef.compareTo(A^.clef,c) > 0 
                   then Result := attribut(c,A^.g,at)
                   else Result := attribut(c,A^.d,at)
     end ;
     
end.
