unit UChap08 ;
{$H-}
interface

	function _copy(ch : String ; p,n : Byte) : String ;
	function _length(s : String) : Byte ;
	procedure _insert(ss : String ; var ch : String ; p : byte) ;
	procedure _delete(var ch : String ; p, n : byte) ;
	function _pos(ss, ch : String) : Integer ;

implementation

	function _copy(ch : String ; p,n : Byte) : String ;
	var k, lg : Byte ;
	begin
		Result := '' ;
		lg := length(ch) ;
		k := 0 ;
		while (k+p <= lg) and (k < n) do 
		 begin
		 	Result := Result + ch[k+p] ;
		 	inc(k)
		 end
	end ;
	
	function _length(s : String) : Byte ;
	begin
		Result := ord(s[0]) ;
	end ;
	
	procedure _insert(ss : String ; var ch : String ; p : byte) ;
	begin
		ch := _copy(ch,1,p-1) + ss + _copy(ch,p,_length(ch)-p+1)
	end ;
	
	procedure _delete(var ch : String ; p, n : byte) ;
	begin
		ch := _copy(ch,1,p-1) + _copy(ch,p+n,_length(ch)-n-p+1)
	end ;
	
	function _pos(ss, ch : String) : Integer ;
	var lgch, lgss , k , ich, iss : Integer ;
	begin
		lgch := _length(ch) ;
		lgss := _length(ss) ;
		Result := 0 ;
		k := 1 ;
		while(k + lgss - 1 <= lgch) do
		  begin
		  	ich := k ;
		  	iss := 1 ;
		  	while(iss<=lgss)and(ch[ich]=ss[iss]) do
		  	  begin
		  	  	inc(ich) ;
		  	  	inc(iss)
		  	  end ;
		  	if iss > lgss then
		  	  begin
		  	  	Result := k ;
		  	  	break
		  	  end ;
		  	inc(k)
		  end 
	end ;
	
end.