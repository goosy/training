program TestString ;
{$H-}
uses UChap08 ;

var S1, S2, S3 : String ;

begin
	S1 := 'ABCDEFGHIJKLMNOPQRST' ;
	S2 := copy(S1,5,10) ;
	S3 :=_copy(S1,5,10) ;
	writeln(S2:20, S3:20) ;
	
	S1 := 'ABCDEFGHIJKLMNOPQRST' ;
	S2 := S1 ;
	S3 := S1 ;
        insert('xyz',S2,10) ;
	_insert('xyz',S3,10) ;
	writeln(S2:30, S3:30) ;
	
	delete(S2,5,10) ;
	_delete(S3,5,10) ;
	writeln(S2:30, S3:30) ;
	
	S1 := 'ABCDEFGHIJKLMNOPQRST' ;
	S2 := S1 ;
	S3 := S1 ;
	writeln(pos('EFGHIJK',S2):10,_pos('EFGHIJK',S3):10) ;
	
	// readln
end.