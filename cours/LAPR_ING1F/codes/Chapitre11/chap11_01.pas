program chap11_01 ;
// {$APPTYPE CONSOLE}

const
     INDMIN = 1 ;
     INDMAX = 5 ;

type
    TEnreg =  record
                    clef : Integer ;
                    data : String ;
              end ;
    TabEnreg = array[INDMIN..INDMAX] of TEnreg ;



procedure affiche(const E : TEnreg) ; overload ;
begin
  with E do
     writeln('clef : ',clef:5,'data : ':20,data) ;
end ;

procedure affiche(const T : array of TEnreg);overload;
var k : Cardinal ;
begin
  for k := low(T) to high(T) do affiche(T[k]) ;
  writeln
end ;

function dicho
(  const T : TabEnreg ;
   indDeb, indFin : Integer ;
   el : Integer ;
   out indEl : Integer
) : Boolean ;

 begin
      Result := False ;
      while (indDeb <= indFin) and not Result do
        begin
           indEl := (indDeb + indFin) div 2 ;
           if T[indEl].clef = el then Result := True
           else if T[indEl].clef < el 
                               then indDeb := indEl + 1
           else {T[indEl].clef>el}  indFin := Indel - 1
        end
 end ;

 const
      TABREC : TabEnreg = ( (clef:1;data:'un') ,
                            (clef:2;data:'deux') ,
                            (clef:3;data:'trois') ,
                            (clef:4;data:'quatre') ,
                            (clef:5;data:'cinq') )  ;

var T : TabEnreg ; cleRecherche : Integer ;
    ou : Integer ; 
begin
   T := TABREC ;
   affiche(T) ;
   repeat
     write('cle de recherche : ') ;
     readln(cleRecherche) ;
     if (cleRecherche = 0) then break ;
     if dicho(T,INDMIN,INDMAX,cleRecherche,ou) 
    then affiche(T[ou])
     else writeln('ELEMENT NON TROUVE !') ;
     writeln
   until False ;
   writeln('TRAITEMENT TERMINE...') ;
   //readln
end.
