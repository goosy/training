program simpleTable ;
uses UTable ;

var dico : Table ;
    francais, anglais : String ;
begin
  // construction du dico...
  create(dico) ;
  repeat
    write('entrez un mot en fran�ais : ') ;
    readLn(francais) ;
  if francais='' then break ;
    write('traduction : ') ;
    readLn(anglais) ; 
    add(francais,anglais,dico) 
  until false ;
  //.......
  writeLn ; writeLn ;
  // utilisation du dico...
  repeat
    write('entrez un mot : ') ;
    readLn(francais) ;
    if francais = '' then break ;
    if attribut(francais,dico,anglais) then
      writeln('traduction de '+francais+' est '+anglais)
    else
      writeLn('ce mot ne se trouve pas dans le dictionnaire')
  until false ;
end.  
    