unit UListe ;
// implémentation d'une liste dans le cas particulier
// d'une table de hachage...
interface

uses UAttribut, UClef ;

type 
     Liste = ^Cellule ;
     Cellule = record
       clef     : TClef ;
       attribut : TAttribut ;
       svt      : Liste
     end{Cellule} ;
     
     procedure create(out L : Liste ) ;
     function  isEmpty( L : Liste ) : Boolean ;
     function  contains(c : TClef; L : Liste ):Boolean ; 
     procedure add(c : TClef; a : TAttribut ; var L : Liste) ;
     procedure delete(c : TClef; var L : Liste) ;
     function attribut(c:TClef;L:Liste;out A : TAttribut):Boolean ;
          
implementation

     procedure create(out L : Liste ) ;
     begin
          L := nil
     end ;

     function isEmpty( L : Liste ) : Boolean ;
     begin
          Result := L = nil
     end ;
     
     function  contains(c : TClef; L : Liste ):Boolean ; 
     begin
       if L = nil then Result := false
       else if UClef.equals(L^.clef,c) then Result := true
       else Result := contains(c,L^.svt)
     end ;
     
     procedure add(c : TClef ; a : TAttribut ; var L:Liste) ;
     begin
         if L = nil then
           begin
              new(L) ;
              UClef.copy(L^.clef,c) ;
              UAttribut.copy(L^.attribut,a) ;
              L^.svt := nil
           end
         else if UClef.equals(c,L^.clef) then
              UAttribut.copy(L^.attribut,a)
         else add(c,a,L^.svt)
     end ;
     
     procedure delete(c : TClef; var L : Liste) ;
     var poubelle : Liste ;
     begin
       if (L<>nil) then
         if UClef.equals(c,L^.clef) then 
         begin
           poubelle := L ;
           L := L^.svt ;
           dispose(poubelle)
         end
         else delete(c,L^.svt)
     end ;
     
     function attribut(c:TClef;L:Liste;out A : TAttribut):Boolean ;
     begin
        if L = nil then Result := false
        else if UClef.equals(L^.clef,c) then 
             begin
               Result := true ;
               UAttribut.copy(A,L^.attribut)
             end
        else Result := attribut(c,L^.svt,A)
     end ;
     
end.
