unit UTable;

interface

uses UListe, UAttribut, UClef ;

const  N = 10 ; //par exemple

type   Indice = 1..N ;
       Table = array[Indice] of Liste ;

       procedure create(out T : Table) ;
       procedure add(c:TClef;a:TAttribut;var T:Table) ;
       function contains(c:TClef ; const T : Table) : Boolean;
       procedure delete(c:TClef; var T : Table) ;
       function attribut(c:TClef; const T : Table; out A : TAttribut):Boolean;
       
implementation

       procedure create(out T : Table) ;
       var k : Indice ;
       begin
            for k := 1 to N do UListe.create(T[k]) ;
       end ;

       function hachage(c : TClef) : Indice ;
       begin
          Result := clefToCardinal(c) mod N + 1
       end ;

       procedure add(c:TClef;a:TAttribut;var T:Table) ;
       begin
         UListe.add(c,a,T[hachage(c)])
       end ;
              
       function contains(c:TClef ; const T : Table) : Boolean; 
       begin
         Result := UListe.contains(c,T[hachage(c)])
       end ;
       
       procedure delete(c:TClef; var T : Table) ;
       begin
         UListe.delete(c,T[hachage(c)])
       end ;
       
       function attribut(c:TClef; const T : Table; out A : TAttribut):Boolean;
       begin
         Result := UListe.attribut(c,T[hachage(c)],A)
       end ;
       
      
       
end.
