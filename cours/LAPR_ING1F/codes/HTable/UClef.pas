unit UClef;
// dans le cas o� la clef est un String...
interface

//------------------------
uses 	UString ;
type	TClef = String ;
//------------------------
	
   procedure copy(var c : TClef; d : TClef) ;
   function equals(c,d : TClef) : Boolean ;
   function toString(c : TClef) : String ;
   function clefToCardinal(c : TClef) : Cardinal ;

implementation

   procedure copy(var c : TClef; d : TClef) ;
   begin
     UString.copy(c,d)  
   end ;

   function equals(c,d : TClef) : Boolean ;
   begin
     Result := UString.equals(c,d) ;
   end ;

   function toString(c : TClef) : String ;
   begin
     Result := UString.toString(c)
   end ;

   function clefToCardinal(c : TClef) : Cardinal ;
   begin
     Result := UString.clefToCardinal(c)
   end ;

end.  
