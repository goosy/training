unit UAttribut;
// dans le cas o� l'attribut est un String...
interface

//----------------------------
uses 	UString ; 
type 	TAttribut = String ;
//----------------------------

    procedure copy(var a: TAttribut; b: TAttribut) ;
    function equals(a,b : TAttribut) : Boolean ;
    function toString(a : TAttribut) : String ;

implementation

    procedure copy(var a: TAttribut; b: TAttribut) ;
    begin
         UString.copy(a,b)
    end ;

    function equals(a,b : TAttribut) : Boolean ;
    begin
         Result := UString.equals(a,b)
    end ;

    function toString(a : TAttribut) : String ;
    begin
        Result := UString.toString(a)
    end ;


end.
