//USort2.pas
//Pirlot Philippe
//06/05/2003

// r�alisation du tri rapide avec partionnement "hollandais"
unit USort2 ;
interface
uses UComparable ;

  procedure qsort(var T : array of Comparable) ;
  
implementation

  procedure qsort(var T : array of Comparable) ;
  
    procedure pivoter(var i,j : Integer) ;
    // pr� : i < j
    var pivot : Comparable ;
        r : Integer ;
    begin
      r := i ;
      copy(pivot,T[j]) ;
      i := j+1 ;
      dec(j) ;
      while r <= j do
        if compareTo(T[j],pivot) < 0 then
        begin
          swap(T[r],T[j]) ;
          inc(r)
        end
        else if compareTo(T[j],pivot) > 0 then
        begin
          dec(i) ;
          swap(T[j],T[i]) ;
          dec(j)
        end
        else dec(j)
     {od}   
    end ;
    
    procedure sort(g,d : Integer) ;
    // pr� : g < d
    var i,j : Integer ;
    begin
      i := g ; j := d ;
      pivoter(i,j) ;
      if g < j then sort(g,j) ;
      if i < d then sort(i,d)
    end ;
    
  begin
    if low(T) < high(T) then sort(low(T),high(T))
  end ;
  
end.  