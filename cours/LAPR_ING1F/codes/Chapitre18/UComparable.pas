//UComparable.pas
//Pirlot Philippe
//06/05/2003

//le type char en tant que Comparable...

unit UComparable ;
interface
  type	Comparable = char ;
  
  procedure copy(out x : Comparable ; y : Comparable) ;
  function equals(x,y : Comparable) : Boolean ;
  function toString(x : Comparable) : String ;
  function compareTo(x,y : Comparable) : Integer ;
  procedure swap(var x,y : Comparable) ;
  
implementation
  procedure copy(out x : Comparable ; y : Comparable) ;
  begin
    x := y
  end ;
  
  function equals(x,y : Comparable) : Boolean ;
  begin
    Result := x = y
  end ;
  
  function toString(x : Comparable) : String ;
  begin
    Result := x
  end ;
  function compareTo(x,y : Comparable) : Integer ;
  begin
    Result := ord(x) - ord(y)
  end ;
  
  procedure swap(var x,y : Comparable) ;
  var tmp : Comparable ;
  begin
    tmp := x ; x := y ; y := tmp
  end ;
  
end.
  	