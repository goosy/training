//petit programme de test
program testSort2 ;
uses USort2 ;

const LG = 20 ;
type ArrayOfChar = array [1..LG] of Char ;

function toChar : char ;
// pr� : randomize
begin
  Result := Char(ord('A')+random(26)) 
end ;

procedure afficher(T : ArrayOfChar) ;
var k : Integer ;
begin
  for k := 1 to LG do write(T[k]:4) ;
  writeLn
end ;

var T : ArrayOfChar ;
    k : Integer ;

begin
  //initialisation du tableau
  randomize ;
  for k := 1 to LG do T[k] := toChar ;
  //affichage du tableau
  afficher(T) ;
  //tri du tableau
  qsort(T) ;
  //affichage du tableau
  afficher(T) 
end.  