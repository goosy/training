unit UString ;
{$H-}
interface
uses SysUtils ;

  function upperTrim(s : String) : String ;
  { retourne : une copie <majuscule> de la cha�ne S
               dont on aurait supprim� tous les <espace(s)>. }
  
  function deleteChar(c : Char ; s : String) : String ; 
  { retourne : une copie de la cha�ne S
    dont on aurait supprim� toutes les
    occurrences du caract�re c           }
    
  procedure deleteChar_(c : Char ; var s : String) ;
  { effet : supprime de la cha�ne S toutes les
            occurrences du caract�re c         }
           
  
implementation
  function upperTrim(s : String) : String ;
  var k : Byte ;
  begin
    //...
  end ;
  
  function deleteChar(c : Char ; s : String) : String ; 
  var k : Byte ;
  begin
    //...
  end ;
  
  procedure deleteChar_(c : Char ; var s : String) ; 
  var r, w : Byte ;
  begin
    //...
  end ;
  
end.