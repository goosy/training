program StringTest ;
{$H-}
uses UString ;

var S : String ;
begin
  S := 'LE SOLEIL TE CHERCHE COMME UNE OMBRE' ;
  writeLn(S) ;				//LE SOLEIL TE CHERCHE COMME UNE OMBRE
  writeLn(deleteChar(' ',S)) ;		//LESOLEILTECHERCHECOMMEUNEOMBRE
  writeLn(S) ;				//LE SOLEIL TE CHERCHE COMME UNE OMBRE
  deleteChar_(' ',S) ;
  writeLn(S) ;				//LESOLEILTECHERCHECOMMEUNEOMBRE
  
  S := 'Van Den Brandt' ;		//Van Den Brandt
  writeLn(S) ;
  writeLn(upperTrim(S)) ;		//VANDENBRANDT
  
end.