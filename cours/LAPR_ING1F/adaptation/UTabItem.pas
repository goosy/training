unit UTabItem ;
interface
uses UItem , SysUtils ;
const TAILLE_MAX = 20 ;
type  TabItem = record
        tab : array [1..TAILLE_MAX] of Item ;
        dim : 0..TAILLE_MAX
      end{TabItem} ;
      
procedure create(out T : TabItem) ;
function search(const T : TabItem ; indDeb, indFin : Cardinal ;
                x : Item ; out indx : Cardinal) : Boolean ;
                
procedure add(x : Item ; var T : TabItem) ;
procedure print(const T : TabItem) ;
function get(const T : TabItem ; indice : Cardinal) : Item ;

implementation

procedure create(out T : TabItem) ;
begin
  T.dim := 0
end ;

procedure add(x : Item ; var T : TabItem) ;
begin
  if T.dim = TAILLE_MAX then raise Exception.create('plÚnitude...') ;
  inc(T.dim) ;
  T.tab[T.dim] := x
end ;

function get(const T : TabItem ; indice : Cardinal) : Item ;
begin
  if (indice = 0)or(indice > T.dim) then raise Exception.create('hors norme...') ;
  Result := T.tab[indice]
end ;



function search(const T : TabItem ; indDeb, indFin : Cardinal ;
                x : Item ; out indx : Cardinal) : Boolean ;
                
begin
  Result := false ;
  while (indDeb <= indFin)and not Result do
  begin
    indx := (indDeb+indFin) div 2 ;
    if UItem.compare(T.tab[indx],x)<0 then indDeb := indx + 1
    else if UItem.compare(T.tab[indx],x)>0 then indFin := indx - 1
    else Result := true
  end
end ;

procedure print(const T : TabItem) ;
var k : Cardinal ;
begin
  for k := 1 to T.dim do ecrire(T.tab[k]) ;
  writeLn
end ;

end.