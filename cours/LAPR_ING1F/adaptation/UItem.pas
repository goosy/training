unit UItem ;
interface

type Item = record 
       matricule : Cardinal ;
       data      : String   ;
     end{Item} ;
procedure lire(out x : Item) ;
procedure ecrire(const x : Item) ;
function compare(a, b : Item) : Integer ;
function create(m : Cardinal) : Item ; overload ;
function create(m : Cardinal;d : String) : Item ; overload ;

implementation

function create(m : Cardinal) : Item ;
begin
  Result.matricule := m ;
  Result.data := ''
end ;

function create(m : Cardinal;d : String) : Item ;
begin
  Result.matricule := m ;
  Result.data := d
end ;

procedure lire(out x : Item) ;
begin
  write('matricule : ') ; readLn(x.matricule) ;
  write('data : ') ; readLn(x.data)
end ;

procedure ecrire(const x : Item) ;
begin
  writeLn('[',x.matricule,' : '+x.data,']')
end ;

function compare(a, b : Item) : Integer ;
begin
  Result := a.matricule - b.matricule
end ;

end.