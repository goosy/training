CREATE TABLE EXECUTIFS (NUM_CERT number(3) NOT NULL,
        NOM Varchar(25),
        ADRESSE Varchar(30),
        FORTUNE number(7),
constraint pk_executifs PRIMARY KEY (NUM_CERT));

CREATE TABLE FILMS (TITRE Varchar(30) NOT NULL,
        ANNEE varchar(4) NOT NULL,
        LONGUEUR INTEGER,
        COULEUR Varchar(3),
        PRODUCTEUR number(3),
        STUDIO Varchar(25),
constraint pk_films PRIMARY KEY (TITRE, ANNEE));

CREATE TABLE JOUE_DANS (TITRE Varchar(30) NOT NULL,
        ANNEE varchar(4) NOT NULL,
        ID_STAR Varchar(25) NOT NULL,
constraint pk_joue_dans  PRIMARY KEY (TITRE, ANNEE, ID_STAR));

CREATE TABLE STARS (ID_STAR Varchar(25) NOT NULL,
        ADRESSE Varchar(30),
        SEXE Varchar(1),
        DDN date,
constraint pk_stars PRIMARY KEY (ID_STAR));

CREATE TABLE STUDIOS (NOM Varchar(25) NOT NULL,
        ADRESSE Varchar(30),
        PRESIDENT number(3),
constraint pk_studios  PRIMARY KEY (NOM));

ALTER TABLE JOUE_DANS ADD constraint fk_joue_dans_films FOREIGN KEY (TITRE, ANNEE) REFERENCES FILMS(TITRE, ANNEE);
ALTER TABLE JOUE_DANS ADD constraint fk_joue_dans_stars FOREIGN KEY (ID_STAR) REFERENCES STARS(ID_STAR);
ALTER TABLE FILMS ADD constraint fk_films_executifs FOREIGN KEY (PRODUCTEUR) REFERENCES EXECUTIFS(NUM_CERT);
ALTER TABLE FILMS ADD constraint fk_films_studios FOREIGN KEY (STUDIO) REFERENCES STUDIOS(NOM);
ALTER TABLE STUDIOS ADD constraint fk_studios_executifs FOREIGN KEY (PRESIDENT) REFERENCES EXECUTIFS(NUM_CERT);

insert into executifs values (3,'JEANNE LENOIR','RUE BLANCHE',200000);
insert into executifs values (5,'BERNARD LEJAUNE','RUE DE LA VIOLETTE',300000);
insert into executifs values (4,'ROGER LEROUGE','RUE BLEUETTE',1500000);
insert into executifs values (6,'SOPHIE BRUN','RUE VERTE',50000);
insert into executifs values (2,'PAUL BLANC','RUE DUMONT',200000);
insert into executifs values (8,'EDITH LEVERT','RUE DUMONT',150000);

insert into stars values ('WHOOPI GOLDBERG','RUE DE LA JOIE','F','16/5/1958');
insert into stars values ('ANTONY HOPKINS','RUE DU TALENT','M','12/8/1939');
insert into stars values ('ROBIN RENUCCI','RUE DE LA DETENTE','M','28/2/1958');
insert into stars values ('FLORENCE PERNEL','RUE DE LA GENTILESSE','F','16/5/1973');
insert into stars values ('JACK LEMMON','RUE DE LA GRIMACE','M','26/11/1939');
insert into stars values ('RICHARD BURTON','RUE DE L''IVRESSE','M','16/5/1932');
insert into stars values ('ELISABETH TAYLOR','RUE DE L''IVRESSE','F','10/4/1934');
insert into stars values ('ROBERTO BEGNINI','RUE DU PITRE','M','25/7/1945');
insert into stars values ('JULIENNE MOORE','RUE POTAGERE','F','9/3/1958');
insert into stars values ('ROGER MOORE','RUE DE L''ESPION','M','12/8/1939');
insert into stars values ('DEMI MOOR','RUE DU GRAND FRISSON','F','1/4/1967');

insert into studios values('NEW LINE CINEMA','RUE DE L''IMAGE 25',5);
insert into studios values('SONY PICTURES','RUE DU COLORIAGE 12',4);
insert into studios values('ERATO FILMS','RUE DE L''ERREUR 14',3);
insert into studios values('UNIVERSAL PICTURES','RUE DE L''ASTRONOMIE 2',8);
insert into studios values('RIGOLO FILMS','RUE DU GAG 13',5);

insert into films values('ANGELIQUE','1978',95,'NON',2,'UNIVERSAL PICTURES');
insert into films values('HISTOIRE SANS FIN','1985',121,'OUI',3,'NEW LINE CINEMA');
insert into films values('HISTOIRE SANS FIN','1999',95,'OUI',3,'NEW LINE CINEMA');
insert into films values('LA BONNE ETOILE','1988',105,'OUI',6,'RIGOLO FILMS');
insert into films values('LES ETOILES DU CIEL','1984',125,'OUI',5,'SONY PICTURES');
insert into films values('LES ETOILES DU CIEL','1999',108,'OUI',5,'UNIVERSAL PICTURES');
insert into films values( 'UNE ETOILE EST NEE','1964',89,'NON',6,'NEW LINE CINEMA');
insert into films values('ANGELIQUE','1999',78,'OUI',3,'SONY PICTURES');

insert into joue_dans values ('ANGELIQUE','1999','FLORENCE PERNEL');
insert into joue_dans values ('ANGELIQUE','1999','ROBIN RENUCCI');
insert into joue_dans values ('ANGELIQUE','1999','DEMI MOOR');
insert into joue_dans values ('ANGELIQUE','1999','ROBERTO BEGNINI');

insert into joue_dans values ('ANGELIQUE','1978','ROBERTO BEGNINI');
insert into joue_dans values ('ANGELIQUE','1978','RICHARD BURTON');
insert into joue_dans values ('ANGELIQUE','1978','ELISABETH TAYLOR');
insert into joue_dans values ('ANGELIQUE','1978','ROGER MOORE');


insert into joue_dans values ('HISTOIRE SANS FIN','1985','ROBERTO BEGNINI');
insert into joue_dans values ('HISTOIRE SANS FIN','1985','WHOOPI GOLDBERG');
insert into joue_dans values ('HISTOIRE SANS FIN','1985','JACK LEMMON');

insert into joue_dans values ('HISTOIRE SANS FIN','1999','FLORENCE PERNEL');
insert into joue_dans values ('HISTOIRE SANS FIN','1999','ROBERTO BEGNINI');


insert into joue_dans values ('LA BONNE ETOILE','1988','ROBERTO BEGNINI');
insert into joue_dans values ('LA BONNE ETOILE','1988','WHOOPI GOLDBERG');
insert into joue_dans values ('LA BONNE ETOILE','1988','ROBIN RENUCCI');
insert into joue_dans values ('LA BONNE ETOILE','1988','ANTONY HOPKINS');
insert into joue_dans values ('LA BONNE ETOILE','1988','JACK LEMMON');
insert into joue_dans values ('LA BONNE ETOILE','1988','RICHARD BURTON');
insert into joue_dans values ('LA BONNE ETOILE','1988','FLORENCE PERNEL');
insert into joue_dans values ('LA BONNE ETOILE','1988','ELISABETH TAYLOR');
insert into joue_dans values ('LA BONNE ETOILE','1988','DEMI MOOR');
insert into joue_dans values ('LA BONNE ETOILE','1988','ROGER MOORE');
insert into joue_dans values ('LA BONNE ETOILE','1988','JULIENNE MOORE');

insert into joue_dans values ('LES ETOILES DU CIEL','1984','ROGER MOORE');
insert into joue_dans values ('LES ETOILES DU CIEL','1984','JACK LEMMON');
insert into joue_dans values ('LES ETOILES DU CIEL','1984','ROBERTO BEGNINI');

insert into joue_dans values ('LES ETOILES DU CIEL','1999','ROGER MOORE');
insert into joue_dans values ('LES ETOILES DU CIEL','1999','JACK LEMMON');
insert into joue_dans values ('LES ETOILES DU CIEL','1999','ROBERTO BEGNINI');

commit;
