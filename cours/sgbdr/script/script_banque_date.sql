create table clientele (
id_client varchar(4),
nom varchar(10),
ville varchar(12),
constraint pk_clientele primary key(id_client)
);
create table agence (
id_agence varchar(4),
ville varchar(12),
constraint pk_agence primary key(id_agence)
);

create table pret_hypothecaire (
id_pret_hypothecaire varchar(4),
client varchar(4),
agence varchar(4),
montant number(5),
date_pret_hyp date,
constraint pk_pret_hypothecaire primary key(id_pret_hypothecaire),
constraint fk_pret_hypothecaire_clientele foreign key(client) references clientele(id_client),
constraint fk_pret_hypothecaire_agence foreign key(agence) references agence(id_agence)
);
create table pret_voiture (
id_pret_voiture varchar(4),
client varchar(4),
agence varchar(4),
montant number(5),
date_pret_voit date,
constraint pk_pret_voiture primary key(id_pret_voiture),
constraint fk_pret_voiture_clientele foreign key(client) references clientele(id_client),
constraint fk_pret_voiture_agence foreign key(agence) references agence(id_agence)
);
insert into clientele values('CL01','PAUL','BRUXELLES');
insert into clientele values('CL02','PIERRE','BRUXELLES');
insert into clientele values('CL03','JACQUES','LOUVAIN');
insert into clientele values('CL04','HENRY','ANVERS');
insert into clientele values('CL05','JACQUES','GAND');
insert into agence values('AG01','BRUXELLES');
insert into agence values('AG02','LIEGE');
insert into agence values('AG03','ANVERS');
insert into pret_hypothecaire values('HY01','CL01','AG01',15000,'16.07.2009');
insert into pret_hypothecaire values('HY02','CL01','AG03',20000,'05.08.2010');
insert into pret_hypothecaire values('HY03','CL02','AG01',40000,'23.02.2008');
insert into pret_hypothecaire values('HY04','CL03','AG01',20000,'13.03.2007');
insert into pret_hypothecaire values('HY05','CL03','AG03',20000,'26.12.2007');
insert into pret_hypothecaire values('HY06','CL05','AG01',12000,'19.04.2005');
insert into pret_hypothecaire values('HY07','CL01','AG02',15000,'11.09.2003');
insert into pret_voiture values('PV01','CL04','AG01',8500,'20.10.2010');
insert into pret_voiture values('PV02','CL03','AG03',7800,'06.10.2008');
insert into pret_voiture values('PV3','CL04','AG03',9000,'06.06.2009');
insert into pret_voiture values('PV04','CL02','AG03',8500,'08.12.2010');
insert into pret_voiture values('PV05','CL03','AG03',7000,'16.07.2009');
commit