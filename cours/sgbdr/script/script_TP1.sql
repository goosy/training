create table Personne (
id_Personne number(5),
nom char(30),
constraint pk_Personne primary key(id_Personne)
) ;

create table Professeur (
id_Prof number(3),
nom char(30),
constraint pk_professeur primary key(id_Prof)
) ;

create table Formation (
id_Formation number(2),
cours char(15),
titulaire number(3),
constraint pk_formation primary key(id_Formation),
constraint fk_formation_professeur foreign key (titulaire) 
references professeur(id_Prof)

) ;

create table suivre (
Formateur number(3),
stagiaire number(5),
constraint pk_suivre primary key(formateur,stagiaire),
constraint fk_suivre_professeur foreign key (formateur) 
references professeur(id_Prof),
constraint fk_suivre_personne foreign key (stagiaire) 
references personne(id_Personne)

) ;


create sequence seqPersonne ;
create sequence seqProfesseur ;
create sequence seqFormation ;


insert into personne values(seqPersonne.nextval,'PAUL') ;
insert into personne values(seqPersonne.nextval,'PIERRE') ;
insert into personne values(seqPersonne.nextval,'JULES') ;
insert into professeur values(seqProfesseur.nextval,'ANDRE') ;
insert into professeur values(seqProfesseur.nextval,'JACQUES') ;
insert into formation values(seqFormation.nextval,'ANALYSE',2);
insert into formation values(seqFormation.nextval,'SQL',2);
insert into formation values(seqFormation.nextval,'COBOL',1);
insert into suivre values(1,2);
insert into suivre values(1,3);
insert into suivre values(2,2);
commit;