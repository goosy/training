program tp6_ex1;
var croissant:boolean; // vrai si la s�quence avant x est croissante, false sinon
    x,precedentDeX,nombreDeNombreAvantX :integer;
  begin
   readln(precedentDeX);
   readln(x);
   nombreDeNombreAvantX:=1;
   croissant:=true;
   while (x<>-1) and croissant
      do begin
          if x< precedentDeX then croissant:=false
                             else begin
                                   precedentDeX:=x;
                                   readln(x);
                                   inc(nombreDeNombreAvantX);
                                  end;
        end;        
   if not croissant then writeln('il y a ', nombreDeNombreAvantX, ' nombres avant la rupture de croissance')
   else writeln('il n''y a pas de c�croissance');
   end.