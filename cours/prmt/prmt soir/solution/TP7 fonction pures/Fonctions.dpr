program Fonctions;

{$APPTYPE CONSOLE}

uses
  SysUtils, Dialogs;

Function Modulo (Val1, Val2 : integer) : integer;
 var
  s : integer;
 begin
  if Val1 > 0 then
   s := 1
  else
   begin
    s := -1;
    Val1 := -Val1;
   end;
  while Val1 > val2 do
    Val1 := Val1 - Val2;
  Result := Val1 * s;
 end;

Function rModulo (val1, val2 : integer) : integer;
 begin
  if val1 > val2 then
   result := rModulo(val1-val2, val2)
  else
   Result := Val1;
 end;

Function Fact(Value : integer) : Cardinal;
 begin
  Result := 1;
  while Value <> 0 do
   begin
    Result := Result * Value;
    Dec(Value);
   end;
 end;

Function rFact(value : integer) : cardinal;
 begin
  if value <> 0 then
   Result := value * rFact(value-1)
  else
   result := 1;
 end;


Function Trav4 (m, n : integer) : integer;
 begin
  Result := Fact(m) div (Fact(m-n) * Fact (n));
 end;

Function Multi(val1, val2 : integer) : integer;
 var
  i : integer;
 begin
  Result := 0;
  for i:=1 to val1 do
   Result := Result + val2;
 end;

Function rMulti(val1, val2 : integer) : integer;
 begin
  if val1 <> 0 then
   result := rMulti(val1-1, val2) + val2
  else
   result := 0;
 end;

Function Trav5 (x, exp : integer) : integer;
 var
  i, j, ival : integer;
 begin
  ival := x;
  for i:=1 to exp-1 do
    iVal := rmulti(iVal,x);
 result := iVal;
 end;

function rTrav5(x, exp : integer) : integer;
 begin
  if exp <> 0 then
   Result := rMulti (Trav5(x, exp-1),x)
  else
   Result := 1;
 end;

begin
 //ShowMessage (IntToStr(20 mod 7));
 //ShowMessage (IntToStr(rModulo(20, 7)));
 //showMessage (IntToStr(rFact(5)));
 //showMessage (IntToStr(Multi(5,3)));
 showMessage (IntToStr(Trav5(2,8)));
 showMessage (IntToStr(rTrav5(2,8)));


end.
