<?php
/**
*
* @author Trésor Tshishi Kandolo 
* 
* Point d'entré du programme
*
*/

include('Personnage.php');

$perso = new Personnage(); //instanciation de la class personnage

$perso->parler();

/*
//provoque une erreur car il est interdit d'appeler un attribut privé en dehors de sa class
$perso->_experience;
*/

$perso->gagnerExperience();   // On gagne de l'expérience.
$perso->afficherExperience(); // On affiche la nouvelle valeur de l'attribut.

//combat

$perso1 = new Personnage(); // Un premier personnage
$perso2 = new Personnage(); // Un second personnage
 
$perso1->frapper($perso2); // $perso1 frappe $perso2
$perso1->gagnerExperience(); // $perso1 gagne de l'expérience
 
$perso2->frapper($perso1); // $perso2 frappe $perso1
$perso2->gagnerExperience(); // $perso2 gagne de l'expérience

$perso1->setForce(10);
$perso1->setExperience(2);
 
$perso2->setForce(90);
$perso2->setExperience(58);
 
$perso1->frapper($perso2);  // $perso1 frappe $perso2
$perso1->gagnerExperience(); // $perso1 gagne de l'expérience
 
$perso2->frapper($perso1);  // $perso2 frappe $perso1
$perso2->gagnerExperience(); // $perso2 gagne de l'expérience
 

echo 'Le personnage 1 a ', $perso1->force(), ' de force, contrairement au personnage 2 qui a ', $perso2->force(), ' de force.<br />';
echo 'Le personnage 1 a ', $perso1->experience(), ' d\'expérience, contrairement au personnage 2 qui a ', $perso2->experience(), ' d\'expérience.<br />';
echo 'Le personnage 1 a ', $perso1->degats(), ' de dégâts, contrairement au personnage 2 qui a ', $perso2->degats(), ' de dégâts.<br />';

